﻿using Chessboard.Services;
using NUnit.Framework;

namespace Chessboard.Tests.Services
{
    [TestFixture]
    public class InputValidationServiceTests
    {
        [TestCase(new int[] { 10, 5 }, ExpectedResult = true)]
        [TestCase(new int[] { -1, 5 }, ExpectedResult = false)]
        [TestCase(new int[] { -1 }, ExpectedResult = false)]
        [TestCase(null, ExpectedResult = false)]
        public bool TestInputParametersChecker(int[] inputs)
        {
            return InputValidationService.CheckInputParametersIsValid(inputs);
        }
    }
}
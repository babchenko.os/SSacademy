﻿using Chessboard.Services;
using NUnit.Framework;

namespace Chessboard.Tests.Services
{
    [TestFixture]
    public class InputParcerServiceTests
    {
        [TestCase(new string[] { "1", "2" }, new int[] { 1, 2 })]
        [TestCase(new string[] { "", "2" }, null)]
        [TestCase(new string[] { "a1", " " }, null)]
        public void CheckParcedInputParameters(string[] input, int[] expectedOutput)
        {
            var result = InputParcerService.ParceInputParameters(input);

            if (result == null)
                Assert.Null(expectedOutput);
            else
                Assert.That(result, Is.EquivalentTo(expectedOutput));
        }
    }
}
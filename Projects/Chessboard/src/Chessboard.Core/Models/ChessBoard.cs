﻿namespace Chessboard.Core.Models
{
    public class ChessBoard
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public override string ToString()
        {
            return $"Chessboard {Width}x{Height}";
        }

        public override int GetHashCode()
        {
            return Width + Height;
        }
    }
}
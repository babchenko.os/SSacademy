﻿using Chessboard.Views;

namespace Chessboard
{
    class Program
    {
        static void Main(string[] args)
        {
            var chessboardDrawer = new ChessboardDrawView();
            chessboardDrawer.DrawChessboard(args);
        }
    }
}
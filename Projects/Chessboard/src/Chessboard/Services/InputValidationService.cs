﻿using System.Linq;

namespace Chessboard.Services
{
    public class InputValidationService
    {
        private const int minInputParametersCount = 2;
        private const int minInputValue = 1;

        public static bool CheckInputParametersIsValid(int[] inputs)
        {
            return inputs != null && inputs.Length >= minInputParametersCount && inputs.All(input => input >= minInputValue);
        }
    }
}
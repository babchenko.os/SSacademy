﻿using System;

namespace Chessboard.Services
{
    public class InputParcerService
    {
        public static int[] ParceInputParameters(string[] inputs)
        {
            try
            {
                return Array.ConvertAll(inputs, input => Convert.ToInt32(input));
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
﻿using Chessboard.Core.Models;
using Chessboard.Services;
using System;

namespace Chessboard.Views
{
    public class ChessboardDrawView
    {
        private const int step = 2;
        private const char whiteSquare = ' ';
        private const char blackSquare = '*';

        public void DrawChessboard(string[] args)
        {
            var inputParameters = InputParcerService.ParceInputParameters(args);

            if (inputParameters == null)
            {
                Console.WriteLine("Input parameters can't be recognized");
                return;
            }

            if (!InputValidationService.CheckInputParametersIsValid(inputParameters))
            {
                Console.WriteLine($"Must be at least 2 parameters and bigger then 1");
                return;
            }

            var chessboard = new ChessBoard
            {
                Width = inputParameters[0],
                Height = inputParameters[1]
            };

            PrintChessboard(chessboard);
        }

        private void PrintChessboard(ChessBoard chessboard)
        {
            for (var y = 0; y < chessboard.Height; y++)
            {
                bool previusIsBlack = y % step == 0 ? false : true;

                for (var x = 0; x < chessboard.Width; x++)
                {
                    if (previusIsBlack)
                        Console.Write(whiteSquare);
                    else
                        Console.Write(blackSquare);

                    previusIsBlack = !previusIsBlack;
                }
                Console.Write(Environment.NewLine);
            }
        }
    }
}
﻿using System;

namespace NumberSequence.Core.Services
{
    public static class PowNumbersService
    {
        public static ulong GetNearestSquareValue(double number)
        {
            if (number == 0D)
                return 0UL;

            var highestSquareNumber = (ulong)Math.Sqrt(number);

            if (Math.Pow(highestSquareNumber, 2) == number)
                highestSquareNumber--;

            return highestSquareNumber;
        }
    }
}
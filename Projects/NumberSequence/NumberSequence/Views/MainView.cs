﻿using NumberSequence.Core.Services;
using System;

namespace NumberSequence.Views
{
    class MainView
    {
        private const ulong SequanceStartNumber = 1;

        public void Run(string[] args)
        {
            if (!CheckInputParameters(args))
            {
                Console.WriteLine("Invalid start parameters");
                return;
            }

            var inputNumber = ParceUserInput(args[0]);

            if (!ValidateUserInput(inputNumber))
            {
                Console.WriteLine("Input number is not valid. Must be more then 0");
                return;
            }

            var highestSquareNumber = PowNumbersService.GetNearestSquareValue(inputNumber.Value);

            PrintSequanceOfNumbers(highestSquareNumber);
        }

        private bool CheckInputParameters(string[] input)
        {
            return input.Length > 0;
        }

        private double? ParceUserInput(string number)
        {
            try
            {
                return Convert.ToDouble(number);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool ValidateUserInput(double? inputNumber)
        {
            return inputNumber.HasValue && inputNumber.Value > 0;
        }

        private void PrintSequanceOfNumbers(ulong lastNumber)
        {
            if (lastNumber == 0)
            {
                Console.WriteLine("No numbers found");
                return;
            }

            for (var i = SequanceStartNumber; i <= lastNumber; i++)
                Console.WriteLine(i);
        }
    }
}
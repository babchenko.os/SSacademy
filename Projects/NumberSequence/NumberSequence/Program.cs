﻿using NumberSequence.Views;

namespace NumberSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainView = new MainView();
            mainView.Run(args);
        }
    }
}
﻿using NumberSequence.Core.Services;
using NUnit.Framework;

namespace NumberSequence.Core.Tests.Services
{
    [TestFixture]
    public class PowNumbersServicesTests
    {
        [TestCase(3, ExpectedResult = 1)]
        [TestCase(123.4, ExpectedResult = 11)]
        [TestCase(-12, ExpectedResult = 0)]
        [TestCase(0, ExpectedResult = 0)]
        public ulong TestNearestSquareValue(double numberToTest)
        {
            return PowNumbersService.GetNearestSquareValue(numberToTest);
        }
    }
}
﻿using System;
using System.Linq;
using FibonacciSequence.Core.Services;

namespace FibonacciSequence.Views
{
    public class MainView
    {
        private const int requiredInputParametersCount = 2;
        private const int minimalUserInputValue = 1;

        public void Run(string[] args)
        {
            if (!ValidateStartParameters(args))
            {
                Console.WriteLine("Not enaught input parameters");
                return;
            }

            var userInputs = ParseInputParameters(args);

            if (userInputs == null)
            {
                Console.WriteLine("Input parameters can't be parsed");
                return;
            }

            if (!CheckUserInputNumersIsValid(userInputs))
            {
                Console.WriteLine($"First parameter must be bigger then second, both parameters must be greater then {minimalUserInputValue}");
                return;
            }

            PrintFibanacciNumbersRange(userInputs[0], userInputs[1]);
        }


        private static bool ValidateStartParameters(string[] parameters)
        {
            return parameters.Length >= requiredInputParametersCount;
        }

        private static ulong[] ParseInputParameters(string[] parameters)
        {
            try
            {
                return Array.ConvertAll(parameters, input => (ulong)Convert.ToDouble(input));
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static bool CheckUserInputNumersIsValid(ulong[] userInputs)
        {
            return userInputs[0] < userInputs[1] && userInputs.All(input => input >= minimalUserInputValue);
        }

        private static void PrintFibanacciNumbersRange(ulong startNumber, ulong endNumber)
        {
            Console.WriteLine($"Fibanacci numbers from {startNumber} to {endNumber}:");

            foreach (var number in FibonacciSequenceService.GetFibonacciSequenceFromRange(startNumber, endNumber))
            {
                Console.WriteLine(number);
            }
        }
    }
}
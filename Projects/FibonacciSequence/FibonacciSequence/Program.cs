﻿using FibonacciSequence.Views;

namespace FibonacciSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainView = new MainView();
            mainView.Run(args);
        }
    }
}
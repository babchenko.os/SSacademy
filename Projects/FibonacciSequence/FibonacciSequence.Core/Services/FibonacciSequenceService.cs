﻿using System.Collections.Generic;

namespace FibonacciSequence.Core.Services
{
    public static class FibonacciSequenceService
    {
        private const long fibonacciStartCurretnNumber = 0;
        private const long fibonacciStartPreviusNumber = 1;

        public static IEnumerable<ulong> GetFibonacciSequenceFromRange(ulong startRange, ulong endRange)
        {
            ulong currentNumber = fibonacciStartCurretnNumber;
            ulong previusNumber = fibonacciStartPreviusNumber;

            while (currentNumber <= endRange)
            {
                var temp = currentNumber;

                currentNumber = previusNumber + currentNumber;

                previusNumber = temp;

                if (currentNumber >= startRange && currentNumber <= endRange)
                    yield return currentNumber;
            }

            yield break;
        }
    }
}
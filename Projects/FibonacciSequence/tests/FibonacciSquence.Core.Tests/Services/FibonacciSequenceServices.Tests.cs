﻿using System.Linq;
using FibonacciSequence.Core.Services;
using NUnit.Framework;
namespace FibonacciSquence.Core.Tests.Services
{
    [TestFixture]
    class FibonacciSequenceServices
    {
        [TestCase(1UL, 7UL, new ulong[] { 1ul, 1ul, 2UL, 3UL, 5UL })]
        [TestCase(8UL, 25UL, new ulong[] { 8UL, 13UL, 21UL })]
        [TestCase(0UL, 3UL, new ulong[] { 1ul, 1ul, 2UL, 3UL })]
        public void TestFibonacciSequence(ulong startNumber, ulong endNumber, ulong[] expectedNumbers)
        {
            var fibonacciNumbers = FibonacciSequenceService.GetFibonacciSequenceFromRange(startNumber, endNumber)
                                                                                                          .ToArray();
            Assert.That(fibonacciNumbers, Is.EquivalentTo(expectedNumbers));
        }
    }
}
﻿using SortingTriangles.Core.Models;

namespace SortingTriangles.Core.Services
{
    public class TriangleValidationService
    {
        private const float minimalSideSize = 0.1F;
        private const int minimalNameLength = 3;

        public static bool CheckTriangleIsValid(Triangle tempTriangle)
        {
            return CheckNameIsvalid(tempTriangle.Name) &&
                   CheckSidesIsValid(tempTriangle) &&
                   CheckSquareIsValid(tempTriangle.Square);
        }

        private static bool CheckNameIsvalid(string name)
        {
            return !string.IsNullOrWhiteSpace(name) && name.Length >= minimalNameLength;
        }

        private static bool CheckSidesIsValid(Triangle triangle)
        {
            return triangle.SideA >= minimalSideSize &&
                              triangle.SideB >= minimalSideSize &&
                              triangle.SideC >= minimalSideSize;
        }

        private static bool CheckSquareIsValid(float sqaure)
        {
            return sqaure > 0;
        }
    }
}
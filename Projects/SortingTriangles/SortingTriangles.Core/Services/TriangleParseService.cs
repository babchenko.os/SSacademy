﻿using SortingTriangles.Core.Models;
using System;
using System.Text.RegularExpressions;

namespace SortingTriangles.Core.Services
{
    public class TriangleParseService
    {
        private const int requiredTrianleInputParametersCount = 4;
        private const char trianleInputParametersSplitter = ',';

        public static Triangle ParseTriangleFromString(string source)
        {
            try
            {
                var inputParameters = source.Split(trianleInputParametersSplitter);

                if (inputParameters.Length < requiredTrianleInputParametersCount)
                    return null;

                var name = RemoveWrongCharactersFromUserInput(inputParameters[0]);
                var sideA = Convert.ToSingle(RemoveWrongCharactersFromUserInput(inputParameters[1]));
                var sideB = Convert.ToSingle(RemoveWrongCharactersFromUserInput(inputParameters[2]));
                var sideC = Convert.ToSingle(RemoveWrongCharactersFromUserInput(inputParameters[3]));
                var square = CalculateSqare(sideA, sideB, sideC);

                return new Triangle
                {
                    Name = name,
                    SideA = sideA,
                    SideB = sideB,
                    SideC = sideC,
                    Square = square
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string RemoveWrongCharactersFromUserInput(string input)
        {
            return Regex.Replace(input, @"\s", string.Empty);
        }

        private static float CalculateSqare(float sideA, float sideB, float sideC)
        {
            var p = (sideA + sideB + sideC) / 2;
            return (float)Math.Sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
        }
    }
}
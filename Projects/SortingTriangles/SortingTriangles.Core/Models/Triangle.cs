﻿namespace SortingTriangles.Core.Models
{
    public class Triangle
    {
        public string Name { get; set; }
        public float SideA { get; set; }
        public float SideB { get; set; }
        public float SideC { get; set; }
        public float Square { get; set; }
    }
}
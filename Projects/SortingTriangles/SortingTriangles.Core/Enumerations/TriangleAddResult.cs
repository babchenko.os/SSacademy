﻿namespace SortingTriangles.Core.Enumerations
{
    public enum TriangleAddResult
    {
        Success,
        UnsupportedInput,
        WrongParameters,
        AlreadyExist
    }
}
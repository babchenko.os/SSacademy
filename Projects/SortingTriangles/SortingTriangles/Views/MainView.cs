﻿using SortingTriangles.Core.Enumerations;
using SortingTriangles.Core.Models;
using SortingTriangles.Core.Services;
using SortingTriangles.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SortingTriangles.Views
{
    public class MainView : IMainView
    {
        private const string userSubmit = "yes";

        private readonly ITriangleController controller;

        public MainView(ITriangleController controller)
        {
            this.controller = controller;
        }

        public void Start()
        {
            while (true)
            {
                var newTriangle = AskUserToEnterNewTriangle();

                var addResult = controller.AddTriangle(newTriangle);

                if (addResult != TriangleAddResult.Success)
                {
                    PrintAddResultErrorMessage(addResult);
                    continue;
                }

                if (!AskUserToAddNewTriangle())
                    break;
            }

            var allTriangles = controller.GetTriangles();
            DisplayTriangles(allTriangles);

            Exit();
        }

        private Triangle AskUserToEnterNewTriangle()
        {
            Console.WriteLine("Type triangle params in format: <Name>, <SideA>, <SideB>, <SideC>");
            var userInput = Console.ReadLine();
            return TriangleParseService.ParseTriangleFromString(userInput);
        }

        private bool AskUserToAddNewTriangle()
        {
            Console.WriteLine(@"Type ""y"" or ""yes"" to add new triangle, otherwise press any other key");
            var userInput = Console.ReadLine();

            return string.Equals(userInput, userSubmit, StringComparison.CurrentCultureIgnoreCase) ||
                   string.Equals(userInput, userSubmit[0].ToString(), StringComparison.CurrentCultureIgnoreCase);
        }

        private void DisplayTriangles(IEnumerable<Triangle> triangles)
        {
            Console.WriteLine("============= Triangles list: ===============");

            var sortedTriangles = triangles
                                     .OrderByDescending(triangle => triangle.Square);

            foreach (var triangle in sortedTriangles)
            {
                Console.WriteLine($"{triangle.Name}:  {triangle.Square}");
            }
        }

        private void PrintAddResultErrorMessage(TriangleAddResult error)
        {
            if (error == TriangleAddResult.AlreadyExist)
                Console.WriteLine("Similar triangle is already added");
            else if (error == TriangleAddResult.UnsupportedInput)
                Console.WriteLine("Input parameters can't be recognized");
            else if (error == TriangleAddResult.WrongParameters)
                Console.WriteLine("Entered wrong parameters");
        }

        private void Exit()
        {
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
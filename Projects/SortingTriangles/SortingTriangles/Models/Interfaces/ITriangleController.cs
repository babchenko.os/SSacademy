﻿using SortingTriangles.Core.Enumerations;
using SortingTriangles.Core.Models;
using System.Collections.Generic;

namespace SortingTriangles.Models.Interfaces
{
    public interface ITriangleController
    {
        TriangleAddResult AddTriangle(Triangle newTriangle);
        IEnumerable<Triangle> GetTriangles();
    }
}
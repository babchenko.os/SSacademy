﻿using SortingTriangles.Controller;
using SortingTriangles.Views;

namespace SortingTriangles
{
    class Program
    {
        static void Main(string[] args)
        {
            var triangleController = new TriangleController();
            var mainView = new MainView(triangleController);
            mainView.Start();
        }
    }
}
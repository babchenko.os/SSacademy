﻿using SortingTriangles.Core.Enumerations;
using SortingTriangles.Core.Models;
using SortingTriangles.Core.Services;
using SortingTriangles.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SortingTriangles.Controller
{
    public class TriangleController : ITriangleController
    {
        private List<Triangle> triangles;

        public TriangleController()
        {
            triangles = new List<Triangle>();
        }

        public TriangleAddResult AddTriangle(Triangle newTriangle)
        {
            if (newTriangle == null)
                return TriangleAddResult.UnsupportedInput;

            if (!TriangleValidationService.CheckTriangleIsValid(newTriangle))
                return TriangleAddResult.WrongParameters;

            if (CheckTriangleIsAlreadyExist(newTriangle))
                return TriangleAddResult.AlreadyExist;

            triangles.Add(newTriangle);
            return TriangleAddResult.Success;
        }

        public IEnumerable<Triangle> GetTriangles()
        {
            return triangles;
        }

        private bool CheckTriangleIsAlreadyExist(Triangle newTriangle)
        {
            return triangles
                    .Any(triangle => triangle.Name == newTriangle.Name ||
                                    (newTriangle.SideA == triangle.SideA &&
                                     newTriangle.SideB == triangle.SideB &&
                                     newTriangle.SideC == triangle.SideC));
        }
    }
}
﻿using NUnit.Framework;
using SortingTriangles.Core.Services;

namespace SortingTriangles.Core.Tests.Services
{
    [TestFixture]
    public class TriangleParseServiceTests
    {
        [TestCase("1,1,1,1", ExpectedResult = true)]
        [TestCase(",1,1,1", ExpectedResult = true)]
        [TestCase("1,1,1", ExpectedResult = false)]
        [TestCase("1,1,b,1", ExpectedResult = false)]
        [TestCase("1,1,,1", ExpectedResult = false)]
        [TestCase("1 , 1 , 1.05 , 1", ExpectedResult = true)]
        public bool TestParseStringToTriangleModel(string input)
        {
            var triangle = TriangleParseService.ParseTriangleFromString(input);
            return triangle != null;
        }
    }
}
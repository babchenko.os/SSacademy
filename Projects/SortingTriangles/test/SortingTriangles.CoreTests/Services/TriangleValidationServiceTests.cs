﻿using NUnit.Framework;
using SortingTriangles.Core.Models;
using SortingTriangles.Core.Services;

namespace SortingTriangles.CoreTests.Services
{
    [TestFixture]
    public class TriangleValidationServiceTests
    {
        [TestCase("Vasya", 1, 1.5F, 1, 1, ExpectedResult = true)]
        [TestCase("Vasya", 0, 1, 1, 1, ExpectedResult = false)]
        [TestCase("Vasya", 1, -1, 1, 1, ExpectedResult = false)]
        [TestCase("Vasya", 1, 1, float.NaN, 1, ExpectedResult = false)]
        [TestCase("V", 0, 1, 1, 1, ExpectedResult = false)]
        public bool TestTriangleValidationChecker(string name = null, float sideA = 0, float sideB = 0, float sideC = 0, float square = 0)
        {
            var triangleToCheck = new Triangle
            {
                Name = name,
                SideA = sideA,
                SideB = sideB,
                SideC = sideC,
                Square = square
            };
            return TriangleValidationService.CheckTriangleIsValid(triangleToCheck);
        }
    }
}
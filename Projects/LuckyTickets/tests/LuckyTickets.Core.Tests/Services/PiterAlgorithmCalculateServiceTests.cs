﻿using LuckyTickets.Core.Services;
using NUnit.Framework;

namespace LuckyTickets.Core.Tests.Services
{
    [TestFixture]
    public class PiterAlgorithmCalculateServiceTests
    {
        [TestCase(1000UL, 100UL, ExpectedResult = 0)]
        [TestCase(100000UL, 999999UL, ExpectedResult = 50412)]
        [TestCase(101000UL, 101101UL, ExpectedResult = 9)]
        public int TestContOfLuckTickets(ulong startNumber, ulong endNumber)
        {
            var countAlgorithm = new PiterAlgorithmCalculateService();
            return countAlgorithm.CountLuckyTickets(startNumber, endNumber);
        }
    }
}
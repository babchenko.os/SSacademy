﻿using LuckyTickets.Core.Services;
using NUnit.Framework;

namespace LuckyTickets.Core.Tests.Services
{
    [TestFixture]
    public class MoskowAlgorithmCalculateServiceTests
    {
        [TestCase(1000UL, 100UL, ExpectedResult = 0)]
        [TestCase(1000UL, 10100UL, ExpectedResult = 0)]
        [TestCase(100000UL, 999999UL, ExpectedResult = 50412)]
        [TestCase(100000UL, 100001UL, ExpectedResult = 1)]
        public int TestContOfLuckTickets(ulong startNumber, ulong endNumber)
        {
            var countAlgorithm = new MoskowAlgorithmCalculateService();
            return countAlgorithm.CountLuckyTickets(startNumber, endNumber);
        }
    }
}

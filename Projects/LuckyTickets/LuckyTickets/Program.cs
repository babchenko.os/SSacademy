﻿using LuckyTickets.Views;

namespace LuckyTickets
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainView = new MainView();
            mainView.Run(args);
        }
    }
}
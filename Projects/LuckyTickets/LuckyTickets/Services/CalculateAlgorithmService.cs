﻿using LuckyTickets.Core.Enumerations;
using LuckyTickets.Core.Services;
using LuckyTickets.Core.Services.Interfaces;
using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace LuckyTickets.Services
{
    public static class CalculateAlgorithmService
    {
        public static ILuckyTicketAlgorithm GetLuckyTicketAlgorithmByFile(string pathToFile)
        {
            var fileContent = File.ReadAllText(pathToFile);

            var filteredContent = FilterFileContent(fileContent);

            if (string.IsNullOrWhiteSpace(filteredContent))
                return null;

            if (!Enum.TryParse(filteredContent, out LuckyTicketAlgorithmType algorithmType))
                return null;

            if (algorithmType == LuckyTicketAlgorithmType.Moskow)
                return new MoskowAlgorithmCalculateService();
            else
                return new PiterAlgorithmCalculateService();
        }

        private static string FilterFileContent(string fileConternt)
        {
            var conterToLower = Regex.Replace(fileConternt.ToLower(), @"\W", string.Empty);
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(conterToLower);
        }
    }
}
﻿using System;
using System.IO;
using LuckyTickets.Core.Enumerations;
using LuckyTickets.Core.Services.Interfaces;
using LuckyTickets.Services;

namespace LuckyTickets.Views
{
    class MainView
    {
        private const int startLuckyTicketNumber = 100000;
        private const int endLuckyTicketNumber = 999999;

        public void Run(string[] args)
        {
            Console.WriteLine("Enter file name:");

            var userInput = Console.ReadLine();

            if (!File.Exists(userInput))
            {
                Console.WriteLine("Can't find file");
                return;
            }

            var calculateAlgorithm = CalculateAlgorithmService.GetLuckyTicketAlgorithmByFile(userInput);

            if (calculateAlgorithm == null)
            {
                Console.WriteLine("Can't decode file content");
                return;
            }

            DisplayLuckyTicketsCount(calculateAlgorithm);
        }

        private void DisplayLuckyTicketsCount(ILuckyTicketAlgorithm ticketAlgorithm)
        {
            if (ticketAlgorithm.AlgorithmType == LuckyTicketAlgorithmType.Moskow)
                Console.WriteLine("Choosed Moskow algorithm");
            else
                Console.WriteLine("Choosed Piter algorithm");

            Console.WriteLine("Count of lucky tikets: ");
            Console.WriteLine(ticketAlgorithm.CountLuckyTickets(startLuckyTicketNumber, endLuckyTicketNumber));
        }
    }
}
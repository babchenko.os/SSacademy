﻿using LuckyTickets.Core.Enumerations;

namespace LuckyTickets.Core.Services.Interfaces
{
    public interface ILuckyTicketAlgorithm
    {
        LuckyTicketAlgorithmType AlgorithmType { get; }
        int CountLuckyTickets(ulong startNumber, ulong endNumber);
    }
}
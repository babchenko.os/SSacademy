﻿using LuckyTickets.Core.Enumerations;
using LuckyTickets.Core.Services.AbstractClasess;
using LuckyTickets.Core.Services.Interfaces;

namespace LuckyTickets.Core.Services
{
    public class MoskowAlgorithmCalculateService : LuckyTicketAlgorithm, ILuckyTicketAlgorithm
    {
        private const int requiredLuckNumberLength = 6;

        public override LuckyTicketAlgorithmType AlgorithmType => LuckyTicketAlgorithmType.Moskow;

        protected override bool CheckNumberIsLucky(int[] digits)
        {
            if (digits.Length != requiredLuckNumberLength)
                return false;

            var leftDigitsSum = 0;
            var rightDigitsSum = 0;

            for (var i = 0; i < digits.Length; i++)
            {
                var digit = digits[i];

                if (i < digits.Length / 2)
                    leftDigitsSum += digit;
                else
                    rightDigitsSum += digit;
            }

            return leftDigitsSum == rightDigitsSum;
        }
    }
}
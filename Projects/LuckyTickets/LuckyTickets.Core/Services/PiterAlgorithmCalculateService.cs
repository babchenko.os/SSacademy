﻿using LuckyTickets.Core.Enumerations;
using LuckyTickets.Core.Services.Interfaces;
using LuckyTickets.Core.Services.AbstractClasess;

namespace LuckyTickets.Core.Services
{
    public class PiterAlgorithmCalculateService : LuckyTicketAlgorithm, ILuckyTicketAlgorithm
    {
        public override LuckyTicketAlgorithmType AlgorithmType => LuckyTicketAlgorithmType.Piter;

        protected override bool CheckNumberIsLucky(int[] digits)
        {
            var oddNumbersCount = 0;
            var evenNumbersCount = 0;

            for (var i = 0; i < digits.Length; i++)
            {
                var digit = digits[i];

                if (i % 2 == 0)
                    evenNumbersCount += digit;
                else
                    oddNumbersCount += digit;
            }

            return oddNumbersCount == evenNumbersCount;
        }
    }
}
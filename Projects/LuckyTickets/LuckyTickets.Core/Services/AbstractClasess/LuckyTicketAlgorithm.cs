﻿using System;
using System.Globalization;
using LuckyTickets.Core.Enumerations;

namespace LuckyTickets.Core.Services.AbstractClasess
{
    public abstract class LuckyTicketAlgorithm
    {
        public abstract LuckyTicketAlgorithmType AlgorithmType { get; }

        public int CountLuckyTickets(ulong startNumber, ulong endNumber)
        {
            var luckyTicketsCount = 0;

            for (var currentNumber = startNumber; currentNumber <= endNumber; currentNumber++)
            {
                var chars = currentNumber.ToString().ToCharArray();
                var digits = Array.ConvertAll(chars, charNumber => CharUnicodeInfo.GetDigitValue(charNumber));

                if (CheckNumberIsLucky(digits))
                    luckyTicketsCount++;
            }

            return luckyTicketsCount;
        }

        protected abstract bool CheckNumberIsLucky(int[] digits);
    }
}
﻿using EnvelopeAnalysis.Services;
using NUnit.Framework;
namespace EnvelopeAnalysis.Tests
{
    [TestFixture]
    public class UnitTest1
    {
        [TestCase("2", ExpectedResult = 2)]
        [TestCase("a", ExpectedResult = null)]
        [TestCase("2.66", ExpectedResult = 2.66F)]
        [TestCase("2. 61", ExpectedResult = 2.61F)]
        [TestCase(".2.1", ExpectedResult = null)]
        public float? ParseStringToEnvelopeSideTest(string source)
        {
            return EnvelopeParseService.ParseStringToEnvelopeSide(source);
        }
    }
}
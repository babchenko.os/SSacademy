﻿using EnvelopeAnalysis.Views;

namespace EnvelopeAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainView = new MainView();
            mainView.Start();
        }
    }
}
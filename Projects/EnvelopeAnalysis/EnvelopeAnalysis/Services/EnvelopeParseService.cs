﻿using System;
using System.Text.RegularExpressions;

namespace EnvelopeAnalysis.Services
{
    public static class EnvelopeParseService
    {
        private const float minEnvelopeSideSize = 0.1F;

        public static float? ParseStringToEnvelopeSide(string source)
        {
            if (source == null || string.IsNullOrWhiteSpace(source))
                return null;

            var normalizedSource = RemoveWrongCharactersFromSource(source);

            var envelopeSide = ParseEnvelopeSideParameter(normalizedSource);

            if (envelopeSide == null || !CheckEnvelopeSidesIsValid(envelopeSide))
                return null;

            return envelopeSide;
        }

        private static string RemoveWrongCharactersFromSource(string source)
        {
            return Regex.Replace(source, @"\s", string.Empty);
        }

        private static float? ParseEnvelopeSideParameter(string parameter)
        {
            try
            {
                return Convert.ToSingle(parameter);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static bool CheckEnvelopeSidesIsValid(float? envelopeSize)
        {
            return envelopeSize.HasValue && envelopeSize >= minEnvelopeSideSize;
        }
    }
}
﻿using System;
using EnvelopeAnalysis.Core.Models;
using EnvelopeAnalysis.Core.Services;
using EnvelopeAnalysis.Services;

namespace EnvelopeAnalysis.Views
{
    public class MainView
    {
        private const string userSubmit = "yes";

        public void Start()
        {
            while (true)
            {
                var firstEnvelope = ReadEnvelope("first");
                var secondEnvelope = ReadEnvelope("second");

                if (EnvelopeAnalysisService.CheckEnvelopeWillFitInEnvelop(firstEnvelope, secondEnvelope))
                    Console.WriteLine("True. One envelop can fit other");
                else
                    Console.WriteLine("False. One envelop ca't fit other");

                if (!AskUserToEnterNewEnvelops())
                    return;
            }
        }

        private static Envelope ReadEnvelope(string envelopeName)
        {
            Console.WriteLine($"Entering {envelopeName} envelope");

            var envelopeWidth = ReadEnvelopeSide("width");
            var envelopeHeight = ReadEnvelopeSide("height");

            return new Envelope { Width = envelopeWidth, Height = envelopeHeight };
        }

        private static float ReadEnvelopeSide(string sideName)
        {
            Console.WriteLine($"enter {sideName}");

            while (true)
            {
                var userInput = Console.ReadLine();
                var envelopeSide = EnvelopeParseService.ParseStringToEnvelopeSide(userInput);

                if (envelopeSide == null)
                {
                    Console.WriteLine("entered parameter not valid");
                    continue;
                }

                return envelopeSide.Value;
            }
        }

        private static bool AskUserToEnterNewEnvelops()
        {
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine(@"Type ""y"" or ""yes"" to add new envelops, otherwise press any other key");
            var userInput = Console.ReadLine().ToLower();

            return userInput == userSubmit || userInput == userSubmit[0].ToString();
        }
    }
}
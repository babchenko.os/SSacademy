﻿using EnvelopeAnalysis.Core.Models;
using EnvelopeAnalysis.Core.Services;
using NUnit.Framework;

namespace EnvelopeAnalysis.Core.Tests.Services
{
    [TestFixture]
    public class EnvelopeAnalysisServiceTests
    {
        [TestCase(100, 100, 100, 100, ExpectedResult = true)]
        [TestCase(100, 100, 101, 100, ExpectedResult = true)]
        [TestCase(100, 10, 10, 100, ExpectedResult = true)]
        [TestCase(10, 100, 100, 10, ExpectedResult = true)]
        [TestCase(5, 105, 100, 50, ExpectedResult = true)]
        [TestCase(50, 100, 5, 105, ExpectedResult = true)]
        [TestCase(150, 10, 100, 100, ExpectedResult = false)]
        public bool CheckEnvelopeWillFitInEnveloptest(float sideA, float sideB, float sideC, float sideD)
        {
            var firstEnvelope = new Envelope { Width = sideA, Height = sideB };
            var secondEnvelope = new Envelope { Width = sideC, Height = sideD };
            return EnvelopeAnalysisService.CheckEnvelopeWillFitInEnvelop(firstEnvelope, secondEnvelope);
        }
    }
}
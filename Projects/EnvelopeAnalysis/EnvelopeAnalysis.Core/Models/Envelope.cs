﻿namespace EnvelopeAnalysis.Core.Models
{
    public class Envelope
    {
        public float Width { get; set; }
        public float Height { get; set; }
    }
}
﻿using EnvelopeAnalysis.Core.Models;
using System;

namespace EnvelopeAnalysis.Core.Services
{
    public static class EnvelopeAnalysisService
    {
        public static bool CheckEnvelopeWillFitInEnvelop(Envelope firstEnvelope, Envelope secondEnvelope)
        {
            return TryFitFirstEnvelopeItToSecond(firstEnvelope, secondEnvelope) || TryFitFirstEnvelopeItToSecond(secondEnvelope, firstEnvelope);
        }

        private static bool TryFitFirstEnvelopeItToSecond(Envelope firstEnvelope, Envelope secondEnvelope)
        {
            if (CheckEnvelopeWillFitHorizontal(firstEnvelope, secondEnvelope))
                return true;
            else if (CheckEnvelopeWillFitVertically(firstEnvelope, secondEnvelope))
                return true;
            else if (CheckEnvelopeWillFitDiagonally(firstEnvelope, secondEnvelope))
                return true;

            return false;
        }

        private static bool CheckEnvelopeWillFitHorizontal(Envelope firstEnvelope, Envelope secondEnvelope)
        {
            return firstEnvelope.Width >= secondEnvelope.Width && firstEnvelope.Height >= secondEnvelope.Height;
        }

        private static bool CheckEnvelopeWillFitVertically(Envelope firstEnvelope, Envelope secondEnvelope)
        {
            return firstEnvelope.Width >= secondEnvelope.Height && firstEnvelope.Height >= secondEnvelope.Width;
        }

        private static bool CheckEnvelopeWillFitDiagonally(Envelope firstEnvelope, Envelope secondEnvelope)
        {
            var sideA = firstEnvelope.Width > firstEnvelope.Height ? firstEnvelope.Width : firstEnvelope.Height;
            var sideB = firstEnvelope.Height < firstEnvelope.Width ? firstEnvelope.Height : firstEnvelope.Width;

            var sideP = secondEnvelope.Width > secondEnvelope.Height ? secondEnvelope.Width : secondEnvelope.Height;
            var sideQ = secondEnvelope.Height < secondEnvelope.Width ? secondEnvelope.Height : secondEnvelope.Width;

            var powA = (float)Math.Pow(sideA, 2);
            var powP = (float)Math.Pow(sideP, 2);
            var powQ = (float)Math.Pow(sideQ, 2);

            var diagonaledSideLength = ((2 * sideP * sideQ * sideA) + (powP - powQ) * Math.Sqrt(powP + powQ - powA)) / (powP + powQ);

            return (sideP > sideA) && (sideB >= diagonaledSideLength);
        }
    }
}
﻿using System.Text.RegularExpressions;

namespace FileParser.Core.Services
{
    public static class StringReplaceService
    {
        public static int CountWordsMatchInText(string sourceText, string wordToFind)
        {
            return Regex.Matches(sourceText, wordToFind).Count;
        }

        public static string ReplaceWordsinText(string sourceText, string oldWord, string newWord, out int countOfReplaces)
        {
            var stringReplaces = 0;

            var newString = Regex.Replace(sourceText, oldWord, match =>
            {
                stringReplaces++;
                return newWord;
            });

            countOfReplaces = stringReplaces;
            return newString;
        }
    }
}
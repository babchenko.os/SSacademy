﻿using System;
using System.IO;

namespace FileParser.Core.Services
{
    public static class FileParserService
    {
        public static int CountStringsInFile(string pathToFile, string stringToCount)
        {
            int stringMatchCount = 0;
            using (var reader = new StreamReader(pathToFile))
            {
                while (!reader.EndOfStream)
                {
                    var currentLine = reader.ReadLine();
                    stringMatchCount += StringReplaceService.CountWordsMatchInText(currentLine, stringToCount);
                }
            }

            return stringMatchCount;
        }

        public static int ReplaseStringInFile(string pathToFile, string stringToReplase, string newString)
        {
            var currentFileName = Path.GetFileNameWithoutExtension(pathToFile);
            var currentPath = Path.GetDirectoryName(pathToFile);
            var newTempFilePath = Path.Combine(currentPath, $"{currentFileName}_{pathToFile.GetHashCode()}.temp");

            int stringReplases = 0;

            try
            {
                using (var reader = new StreamReader(pathToFile))
                {
                    using (var writer = new StreamWriter(newTempFilePath))
                    {
                        while (!reader.EndOfStream)
                        {
                            var currentLine = reader.ReadLine();

                            var newLine = StringReplaceService.ReplaceWordsinText(currentLine, stringToReplase, newString, out var countOfReplases);
                            stringReplases += countOfReplases;

                            writer.WriteLine(newLine);
                        }
                    }
                }

                File.Replace(newTempFilePath, pathToFile, null);
                return stringReplases;
            }
            catch (Exception)
            {
                File.Delete(newTempFilePath);
                throw;
            }
        }
    }
}
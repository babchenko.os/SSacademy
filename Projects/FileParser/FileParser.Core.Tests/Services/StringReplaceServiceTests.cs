﻿using FileParser.Core.Services;
using NUnit.Framework;

namespace FileParser.Core.Tests.Services
{
    [TestFixture]
    class StringReplaceServiceTests
    {
        [TestCase("1234", "1", ExpectedResult = 1)]
        [TestCase("a b b v  vv", "b", ExpectedResult = 2)]
        [TestCase("pooopossd", "s", ExpectedResult = 2)]
        public int TestCountWordsMatchInText(string text, string stringToCount)
        {
            return StringReplaceService.CountWordsMatchInText(text, stringToCount);
        }

        [TestCase("1234", "1", "NEW", ExpectedResult = "NEW234")]
        [TestCase("a b-b v  vv", "b", "NEW", ExpectedResult = "a NEW-NEW v  vv")]
        [TestCase("pooopossd", "s", "NEW", ExpectedResult = "pooopoNEWNEWd")]
        public string TestReplaceWordinText(string text, string oldString, string newString)
        {
            return StringReplaceService.ReplaceWordsinText(text, oldString, newString, out var countOfReplaces);
        }
    }
}
﻿using FileParser.Views;

namespace FileParser
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainView = new MainView();
            mainView.Run(args);
        }
    }
}
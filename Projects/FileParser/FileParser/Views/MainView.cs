﻿using System;
using System.Linq;
using System.IO;
using FileParser.Core.Services;

namespace FileParser.Views
{
    class MainView
    {
        private const int argumentsCountForCountMode = 2;
        private const int argumentsCountForReplaseMode = 3;

        public void Run(string[] args)
        {
            if (!CheckInputParametersIsValid(args))
            {
                Console.WriteLine("Not enough input arguments");
                return;
            }

            var pathToFile = args[0];

            if (!File.Exists(pathToFile))
            {
                Console.WriteLine($"Can't find file with path {pathToFile}");
                return;
            }

            if (args.Length == argumentsCountForCountMode)
                RunCountStringsInFile(pathToFile, args[1]);
            else
                RunReplaseStringsInFile(pathToFile, args[1], args[2]);
        }

        private bool CheckInputParametersIsValid(string[] parameters)
        {
            return parameters.Length >= argumentsCountForCountMode && parameters.All(parameter => !string.IsNullOrWhiteSpace(parameter));
        }

        private void RunCountStringsInFile(string pathToFile, string stringToCount)
        {
            try
            {
                var result = FileParserService.CountStringsInFile(pathToFile, stringToCount);
                Console.WriteLine($"Founded strings in the file: {result}");
            }
            catch (Exception)
            {
                Console.WriteLine("Count wasn't success");
            }
        }

        private void RunReplaseStringsInFile(string pathToFile, string stringToRaplace, string newString)
        {
            try
            {
                var result = FileParserService.ReplaseStringInFile(pathToFile, stringToRaplace, newString);
                Console.WriteLine($"Replased strings in the file: {result}");
            }
            catch (Exception)
            {
                Console.WriteLine("Replase wasn't success");
            }
        }
    }
}
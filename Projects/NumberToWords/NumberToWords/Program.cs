﻿using NumberToWords.Views;

namespace NumberToWords
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainView = new MainView();
            mainView.Run(args);
        }
    }
}
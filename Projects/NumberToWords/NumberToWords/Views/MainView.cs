﻿using System;
using System.Linq;
using NumberToWords.Core.Services;

namespace NumberToWords.Views
{
    public class MainView
    {
        public void Run(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("No input parameters");
                return;
            }

            var parsedParameters = ParseInputParameters(args);

            foreach (var input in parsedParameters)
            {
                if (!input.HasValue)
                {
                    Console.WriteLine("Value can't be recognized");
                    continue;
                }

                PrintNumberInWords(input.Value);
            }
        }

        private ulong?[] ParseInputParameters(string[] args)
        {
            return args
                    .Select(ParceInputNumer)
                    .ToArray();
        }

        private ulong? ParceInputNumer(string input)
        {
            try
            {
                return Convert.ToUInt64(input);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void PrintNumberInWords(ulong number)
        {
            try
            {
                Console.WriteLine(NumberToWordsConverterService.ParceNumberToWords(number));
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Number is too big");
            }
        }
    }
}
﻿using Newtonsoft.Json;
using NumberToWords.Core.Models;
using System.Collections.Generic;

namespace NumberToWords.Core.Configuration
{
    internal class Settings
    {
        [JsonProperty("numbers_grade")]
        public IEnumerable<NumberGrade> NumbersGrade { get; set; }

        [JsonProperty("numbers_in_words")]
        public IDictionary<ulong, string> NumbersInWords { get; set; }
    }
}
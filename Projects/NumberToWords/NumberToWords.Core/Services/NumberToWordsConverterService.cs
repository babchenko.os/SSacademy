﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using NumberToWords.Core.Configuration;
using NumberToWords.Core.Models;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NumberToWords.Core.Services
{
    public class NumberToWordsConverterService
    {
        private const char worldSplitter = ' ';

        private static Settings settings;

        private StringBuilder output;

        static NumberToWordsConverterService()
        {
            var pathtoConfig = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"settings.json");
            settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(pathtoConfig));
        }

        public static string ParceNumberToWords(ulong number)
        {
            if (number == 0)
                return settings.NumbersInWords[number];

            var numbersGrade = settings.NumbersGrade
                                          .OrderByDescending(grade => grade.Value);

            if (numbersGrade.First().Value * 1000 < number)
                throw new ArgumentOutOfRangeException("Number is too big");

            return ParseNumberGrades(numbersGrade, number);
        }

        private static string ParseNumberGrades(IEnumerable<NumberGrade> numbersGrade, ulong number)
        {
            var output = new StringBuilder();

            foreach (var grade in numbersGrade)
            {
                var gradeValue = (number / grade.Value);

                if (gradeValue > 0)
                {
                    var thouthand = ParceNumberBelowThouthand(gradeValue);
                    AppendNewWord(thouthand, output);

                    if (grade.Name != null)
                        AppendNewWord(grade.Name, output);
                }

                number = number - (gradeValue * grade.Value);
            }

            return output.ToString();
        }

        private static string ParceNumberBelowThouthand(ulong number)
        {
            var output = new StringBuilder();

            var hundreds = number / 100;

            if (hundreds > 0)
            {
                number = number - hundreds * 100;
                AppendNewWord(settings.NumbersInWords[hundreds], output);
                AppendNewWord("hundrends", output);
            }

            if (settings.NumbersInWords.TryGetValue(number, out var tensString))
                AppendNewWord(tensString, output);
            else
            {
                var tens = (number / 10) * 10;
                AppendNewWord(settings.NumbersInWords[tens], output);
                var single = number - tens;
                AppendNewWord(settings.NumbersInWords[single], output);
            }

            return output.ToString();
        }

        private static void AppendNewWord(string word, StringBuilder output)
        {
            if (output.Length > 0)
                output.Append(worldSplitter);

            output.Append(word);
        }
    }
}
﻿namespace NumberToWords.Core.Models
{
    public class NumberGrade
    {
        public ulong Value { get; set; }
        public string Name { get; set; }
    }
}
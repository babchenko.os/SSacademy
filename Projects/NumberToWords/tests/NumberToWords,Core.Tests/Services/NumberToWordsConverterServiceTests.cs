﻿using NumberToWords.Core.Services;
using NUnit.Framework;

namespace NumberToWords_Core.Tests.Services
{
    [TestFixture]
    class NumberToWordsConverterServiceTests
    {
        [TestCase(0UL, ExpectedResult = "zero")]
        [TestCase(1UL, ExpectedResult = "one")]
        [TestCase(1000013UL, ExpectedResult = "one million thirteen")]
        public string TestNumberToWordConverter(ulong numberToTest)
        {
            return NumberToWordsConverterService.ParceNumberToWords(numberToTest);
        }
    }
}